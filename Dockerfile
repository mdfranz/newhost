FROM python:3
RUN mkdir /opt/bin && pip3 install boto3 requests arrow --upgrade
ADD lib/logsearch.py /opt/bin
