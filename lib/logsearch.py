# I looked at https://alexwlchan.net/2017/11/fetching-cloudwatch-logs/

import boto3,re,requests,time,arrow

def lookup_mac(m):
    time.sleep(3)
    url = "https://api.macvendors.com/" + m
    r = requests.get(url)
    c = r.content.decode()

    if c.find("errors") < 0:
        return (c)
    else:
        return("unknown")

def host_tuple(s):
    """Extract Mac address, IP address and hostname if available from L2 logs"""

    dhcp = re.search( '^.+DHCPACK\S+\s(\S+)\s(\S+)\s(\S*)$',s)
    arpwatch = re.search( '.*station\s(\S+)\s(\S+)\s.*$',s)

    if dhcp != None:
        mac = dhcp.group(2)
        ip = dhcp.group(1)
        hostname = dhcp.group(3)
        if hostname == "":
            hostname = "unknown"
        return (mac,ip,hostname)
    elif arpwatch != None: 
        ip = arpwatch.group(1)
        mac = arpwatch.group(2)
        return (mac,ip,"unknown")
    else:
        return None

def update_hostdb(r):
    """Write data to DynamoDB, create new item or updater timestamp"""

    db = boto3.resource('dynamodb',region_name="us-east-2")
    tbl = db.Table("hosts")

    tbl.put_item(
        Item= {
            'macaddr': r[0],
            'ipaddr': r[1],
            'hostname': r[2],
            'vendor': r[3],
            'timestamp': int(r[4])
        }
    )

def extract_hosts(max_entries=1000):
    """Get hosts from DHCP or Arpwatch logs"""
    regions = ["us-east-1","us-east-2"] # TODO could make this do all regions later
    host_list = []
    host_dict = {}
    resolve_dict = {}
    vendor_dict = {}

    # Assumes "/var/log/syslog" log group to be searched
    for r in regions:
        c = boto3.client("logs", region_name=r)

        start = int(time.time() * 1000 - 1000 * 60 * 60 * 2) # 2 hours back
        finish = int( time.time() * 1000 )

        # start with DHCP logs
        lr = c.filter_log_events(logGroupName="/var/log/syslog",limit=max_entries,filterPattern="dnsmasq DHCPACK",
                startTime=start,endTime=finish)

        for e in lr['events']:
            ht = host_tuple( e['message'] )
            resolve_dict[ht[1]] = ht[2]
            pk = ( ht[0], ht[1] ) 
            if pk not in host_dict:
                host_dict[pk] = e['timestamp'] / 1000 # switch back to seconds
            else:
                if e['timestamp'] > host_dict[pk]:
                    host_dict[pk] = e['timestamp'] / 1000 # seconds

            host_list.append((e['message'],e['timestamp']))
       
        lr = c.filter_log_events(logGroupName="/var/log/syslog",limit=max_entries,filterPattern="arpwatch new",
                startTime=start,endTime=finish)

        for e in lr['events']:
            ht = host_tuple(e['message'])
            pk = ( ht[0], ht[1] ) 
            if pk not in host_dict:
                host_dict[pk] = e['timestamp'] / 1000 
            else:
                if e['timestamp'] > host_dict[pk]:
                    host_dict[pk] = e['timestamp'] / 1000

    for k in host_dict.keys():
        if k[0] not in vendor_dict:
            vendor_dict[k[0]] = lookup_mac(k[0])

        if k[1] in resolve_dict:
            print( k[0],k[1],arrow.get(host_dict[k]).humanize(),resolve_dict[k[1]],vendor_dict[k[0]] )
            update_hostdb( ( k[0], k[1], resolve_dict[k[1]], vendor_dict[k[0]], host_dict[k] ) )
        else:
            print(k[0],k[1],arrow.get(host_dict[k]).humanize(),vendor_dict[k[0]])
            update_hostdb( ( k[0], k[1], "unknown", vendor_dict[k[0]], host_dict[k] ) )


if __name__ == "__main__":
    extract_hosts()
