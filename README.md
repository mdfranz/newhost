# What is?

This is a silly project to monitor my home network based on CloudWatch log monitoring L2/L3 actvity

Right now it prints something like this

```
74:70:fd:xxxx 192.168.3.245 an hour ago unknown Intel Corporate
90:32:4b:xxxx 192.168.2.161 an hour ago DESKTOP-XXXX Hon Hai Precision Ind. Co.,Ltd.
98:10:e8:xxxx 192.168.2.249 an hour ago XXXXX-iPhone Apple, Inc.
74:70:fd:xxxx 192.168.50.71 an hour ago unknown Intel Corporate
00:03:7f:xxxx 192.168.2.123 an hour ago unknown Atheros Communications, Inc.
98:10:e8:xxxx 192.168.3.248 43 minutes ago XXXX-iPhone Apple, Inc.
18:81:0e:xxxx 192.168.2.196 28 minutes ago DogFish46 Apple, Inc.
18:81:0e:xxxx 192.168.3.196 6 minutes ago DogFish46 Apple, Inc.
b0:68:e6:xxxx 192.168.2.122 a minute ago DESKTOP-XXXX CHONGQING FUGUI ELECTRONICS CO.,LTD.
74:70:fd:xxxx 192.168.1.184 an hour ago Intel Corporate
48:5d:36:xxxx 192.168.1.1 41 minutes ago Verizon 
00:12:3f:xxxx 192.168.1.41 41 minutes ago Dell Inc.
98:10:e8:xxxx 192.168.1.162 41 minutes ago Apple, Inc.
ac:22:0b:xxxx 192.168.1.154 40 minutes ago ASUSTek COMPUTER INC.
34:23:87:xxxx 192.168.1.181 34 minutes ago Hon Hai Precision Ind. Co.,Ltd.
18:81:0e:xxxx 192.168.1.177 28 minutes ago Apple, Inc.
```

And writes to a DynamoDB table like this
```
    tbl.put_item(
        Item= {
            'macaddr': r[0],
            'ipaddr': r[1],
            'hostname': r[2],
            'vendor': r[3],
            'timestamp': int(r[4])
        }
    )
```

# Assumptions

I'm using a chain of Asus routers that are sending their logs to a Linux box sending those logs to CloudWatch logs

On most subnets I have a Linux box (Pi or an old laptop) running arpwatch. These are also going to CloudWatch logs

# Deployment

The whole point is to come up with something small that will be deployed as Lambda and as a Fargate task to compare the implementation

I will probably use DynamoDB streams to generate SNS notifications for different types of changes but I'm not there yet. 

# Libraries & Sites
- https://api.macvendors.com/ - for looking up MAC vendors
- https://arrow.readthedocs.io/en/latest/ - for human friendly time
- https://boto3.amazonaws.com for CloudWatch Logs and DynamoDB updates
